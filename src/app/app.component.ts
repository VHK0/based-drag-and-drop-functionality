import { Component, ViewChild, ElementRef, OnInit, AfterViewInit, NgZone, ChangeDetectorRef } from '@angular/core';

declare var $: any;

enum ID {
  Play, Say, Collect, Dial, HangUp, Control, ExternalService, Log, Reject, Redirect, Pause, SMS, Email, Record, Print
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {

  leftItems = [
    { id: ID.Play, icon: 'play-circle', text: 'Play' },
    { id: ID.Say, icon: 'quote-left', text: 'Say' },
    { id: ID.Collect, icon: 'th', text: 'Collect' },
    { id: ID.Dial, icon: 'phone', text: 'Dial' },
    { id: ID.HangUp, icon: 'times', text: 'Hang Up' },
    { id: ID.Control, icon: 'cogs', text: 'Control' },
    { id: ID.ExternalService, icon: 'external-link', text: 'External Service' },
    { id: ID.Log, icon: 'align-justify', text: 'Log' },
    { id: ID.Reject, icon: 'thumbs-o-down', text: 'Reject' },
    { id: ID.Redirect, icon: 'link', text: 'Redirect' },
    { id: ID.Pause, icon: 'pause', text: 'Pause' },
    { id: ID.SMS, icon: 'envelope-o', text: 'SMS' },
    { id: ID.Email, icon: 'comment-o', text: 'Email' },
    { id: ID.Record, icon: 'microphone', text: 'Record' },
    { id: ID.Print, icon: 'print', text: 'Print' }
  ];

  rightItems = [
    { id: ID.Play, icon: 'play-circle', text: 'Play' }
  ];

  constructor(private zone: NgZone,
    private cdRef: ChangeDetectorRef) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      $('.draggable').draggable({
        revert: 'invalid',
        connectToSortable: '.sortable',
        helper: 'clone',
        start: (event, ui) => {
          $(ui.helper).addClass('ui-helper');
        }
      });
      $('.sortable').sortable({
        revert: true,
        placeholder: 'ui-state-highlight',
        receive: (event, ui) => {
        },
        update: (_, ui) => {
          const item = ui.item;
          if (item.hasClass('draggable')) {
            const id = item.data('id');
            const index = item.index();
            const _item = this.leftItems.find(x => x.id === id);
            console.log(index);
            this.rightItems.splice(index, 0, _item);
            this.cdRef.detectChanges();
          }
          $('.sortable .draggable').remove();
        }
      });
    });
  }
}
